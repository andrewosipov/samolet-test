import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import { Layout, Spin } from 'antd';
import { getData } from './utils/api';
import Home from './pages/home';
import Details from './pages/details';
import { IData } from './types';

import './app.scss';

export default function App() {
    const [data, setData] = useState<IData[]>([]);
    const [isLoading, setLoading] = useState<boolean>(true);

    useEffect(() => {
    getData().then((data) => {
        setData(data);
        setLoading(false);
    });
    }, []);

    if (isLoading) {
        return <Spin className="loader" tip="Загрузка..." />
    }

    return (
        <Router>
            <Layout className="layout">
                <Layout.Content className="container">
                    <Switch>
                        <Route path="/details/:id" exact>
                            <Details data={data} />
                        </Route>
                        <Route path="/" exact>
                            <Home data={data} />
                        </Route>
                        <Route path="*">
                            <Redirect to="/" />
                        </Route>
                    </Switch>
                </Layout.Content>
            </Layout>
        </Router>
    );
}
