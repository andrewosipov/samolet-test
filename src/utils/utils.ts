import memoizeOne from 'memoize-one';
import { IData, IRegion } from 'types';

const getRegionsArray = (data: IData[]): IRegion[] => {
    return data.map(item => ({
        key: item.kopuk,
        order: item.order,
        name: item.territory,
        libraries: item.libraries,
    }));
}

export const getRegions = memoizeOne(getRegionsArray);
