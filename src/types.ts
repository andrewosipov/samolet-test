/** Информация о записи. */
export interface IData {
    order: number
    fullname: string
    territory: string
    address: string
    libraries: number
    kopuk: string
    formname: string
    period: number
    users: number
    libraries_computers: number
    digital_catalogs: number
}

/** Информация о регионе. */
export interface IRegion {
    key: string
    order: number
    name: string
    libraries: number
}
