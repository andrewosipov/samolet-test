import React, { useCallback } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { Button } from 'antd';
import { IData } from 'types';

interface IProps {
    data: IData[],
}

export default (props: IProps) => {
    const history = useHistory();
    const { id } = useParams();
    const backClickHandler = useCallback(() => history.push('/'), []);
    const details = props.data.find(item => item.kopuk === id);
    const renderContent = (details: IData) => (
        <>
            <h2>#{details.kopuk} {details.formname}</h2>
            <p>
                <strong>Регион: </strong>
                {details.territory}
            </p>
            <p>
                <strong>Орган: </strong>
                {details.fullname}
            </p>
            <p>
                <strong>Адрес: </strong>
                {details.address}
            </p>
            <p>
                <strong>Год: </strong>
                {details.period}
            </p>
            <p>
                <strong>Количество библиотек: </strong>
                {details.libraries}
            </p>
            <p>
                <strong>Количество компьютеров: </strong>
                {Math.round(details.libraries_computers)}
            </p>
            <p>
                <strong>Количество пользователей: </strong>
                {Math.round(details.users)}
            </p>
            <p>
                <strong>Количество каталогов: </strong>
                {Math.round(details.digital_catalogs)}
            </p>
        </>
    );
    let content;

    if (!Boolean(details)) {
        content = (
            <h3>Информация не найдена</h3>
        );
    } else {
        // @ts-ignore
        content = renderContent(details);
    }

    return (
        <div>
            <Button type="primary" onClick={backClickHandler}>Назад</Button>
            <div className="content">
                {content}
            </div>
        </div>
    );
}
