import React, { useState, useCallback, SyntheticEvent } from 'react';
import { useHistory } from 'react-router-dom';
import { Form, Input, Row, Col, Table } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import { cloneDeep } from 'lodash';
import { getRegions } from 'utils/utils';
import { IData, IRegion } from 'types';

/** Пропсы. */
interface IProps {
    data: IData[],
}

/** Параметры сортировки */
interface ISorter {
    order?: string;
}

export default (props: IProps) => {
    const regions: IRegion[] = getRegions(props.data);
    const [tableData, setTableData] = useState<IRegion[]>(regions);
    const history = useHistory();

    const tableChangeHandler = useCallback((pagination: any, filters: any, sorter: ISorter) => {
        if (!Boolean(sorter?.order)) {
            setTableData(regions);
            return;
        }
        const clonnedData = cloneDeep(tableData);
        if (sorter.order === 'ascend') {
            clonnedData.sort((a, b) => a.libraries > b.libraries ? 1 : -1)
        } else {
            clonnedData.sort((a, b) => a.libraries > b.libraries ? -1 : 1)
        }
        setTableData(clonnedData);
    }, [props.data]);

    const onRowHandler = useCallback((record: IRegion, rowIndex: number) => {
        return {
            onClick: (event: SyntheticEvent) => {
                history.push(`/details/${record.key}`);
            },
        };
    }, [props.data]);

    const filterChangeHandler = useCallback((event: React.FormEvent<HTMLInputElement>) => {
        const newFilteredData = tableData.filter(item => item.name.toLowerCase().indexOf(event.currentTarget.value.toLowerCase()) >= 0);
        setTableData(newFilteredData);
    }, [props.data]);

    const filterSubmitHandler = useCallback((event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
    }, [props.data]);

    const columns = [
        {
            title: 'Регион',
            dataIndex: 'name',
            key: 'name',
            ellipsis: true,
        },
        {
            title: 'Библиотеки',
            dataIndex: 'libraries',
            key: 'libraries',
            ellipsis: true,
            sorter: true,
        },
    ];

    return (
        <div>
            <Form
                className="filter"
                onSubmit={filterSubmitHandler}
            >
                <Row>
                    <Col md={7}>
                        <Input
                            type="text"
                            placeholder="Введите название региона"
                            onChange={filterChangeHandler}
                            prefix={<SearchOutlined />}
                        />
                    </Col>
                </Row>
            </Form>
            <Table
                columns={columns}
                dataSource={tableData}
                onChange={tableChangeHandler}
                onRow={onRowHandler}
            />
        </div>
    );
}
